# This project based on [LXQt](https://github.com/lxqt) &  [Slackware](http://www.slackware.com/) 




# Installing


**Building Instructions**

If KDE- Dependencies are not installed:<br><br>
`KDE=yes ./build.sh`

**_Slackware 15.0_**

```
$ git clone https://gitlab.com/slackernetuk/lxqt-for-slackware.git -b stable
$ cd lxqt-for-slackware
$ ./build.sh
```


**_Slackware -current_**

```
$ git clone https://gitlab.com/slackernetuk/lxqt-for-slackware.git -b current
$ cd lxqt-for-slackware
$ ./build.sh
```


**Configuration**



Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

System-wide LXQt configuration files are in /usr/share/lxqt, while user configuration files are in ~/.config/lxqt.
For a standard setup, I recommend to copy the system-wide LXQt configuration files to ~/.config/lxqt

`cp -a /usr/share/lxqt ~/.config/`


`xwmconfig` to setup lxqt as standard-session



**ScreenShot**

![screen](/uploads/a8162eab9f1a0711e4699c7ccd99f5b7/screen.jpg)


